all:searchalgorithms

searchalgorithms: algorithm.o linear.o binary.o
	gcc algorithm.o linear.o binary.o -o searchalgorithms.o

algorithm.o: algorithm.c
	gcc -c algorithm.c

linear.o: linear.c
	gcc -c linear.c

binary.o: binary.c
	gcc -c binary.c
